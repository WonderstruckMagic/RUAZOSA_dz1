package lecture4.ruazosa.fer.hr.calculator

object Calculator {

    var result: Double = 0.0
    private set

    var part_result: Double = 0.0
    private set

    var i: Int = 0
    private set

    var first: Boolean = true
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    var simple_expression: MutableList<String> = mutableListOf()
        private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
        simple_expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun simplify() {
        i=0
        while(i < expression.count()-1){
            if((expression[i+1]=="*" || expression[i+1]=="/")) {
                while (i < expression.count() - 1 && (expression[i + 1] == "*" || expression[i + 1] == "/")) {
                    if (first) {
                        if (expression[i + 1] == "*") {
                            part_result = expression[i].toDouble() * expression[i + 2].toDouble()
                        } else {
                            part_result = expression[i].toDouble() / expression[i + 2].toDouble()
                        }
                        first = false
                    } else {
                        if (expression[i + 1] == "*") {
                            part_result *= expression[i + 2].toDouble()
                        } else {
                            part_result /= expression[i + 2].toDouble()
                        }
                    }
                    i += 2
                }
                simple_expression.add(part_result.toString())
                part_result = 0.0
                first = true
            }else if (i==0 || (expression[i-1]!="*" && expression[i-1]!="/")){
                simple_expression.add(expression[i].toString())
                i++
                if(i== expression.count()-1){
                    simple_expression.add(expression[i].toString())
                }
            }else{
                i++
                if(i== expression.count()-1){
                    simple_expression.add(expression[i].toString())
                }
            }
        }
        expression= simple_expression
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }


        simplify();

        result = expression[0].toDouble()

        for(i in 1..expression.count()- 1 step 2) {
            when(expression[i]) {
                "+" -> result = result + expression[i+1].toDouble()
                "-" -> result = result - expression[i+1].toDouble()
            }
        }
    }
}